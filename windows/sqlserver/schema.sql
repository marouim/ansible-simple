CREATE TABLE clients (id int, name varchar(255));
CREATE TABLE products (id int, name varchar(255), quantity int);
CREATE TABLE bills (id int, name varchar(255));

INSERT INTO products VALUES (1, 'shrimp', 45);
INSERT INTO products VALUES (2, 'scallop', 14);
INSERT INTO products VALUES (3, 'salmon', 23);