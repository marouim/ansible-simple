---
- name: "Install SQL server"
  gather_facts: yes
  hosts: all

  tasks: 

  # == Load required powershell modules ==========================================
  - name: Powershell | Check for SQLServer DSC Powershell module
    win_psmodule:
      name: SQLServerDsc
      state: present

  - name: Powershell | Check for Storage DSC Powershell module
    win_psmodule:
      name: StorageDsc
      state: present

  - name: Powershell | Check for ServerManager Powershell module
    win_psmodule:
      name: ServerManager
      state: present

  - name: Powershell | Ensure that DBA Tools module is present
    win_psmodule:
      name: dbatools
      state: present

  - name: Powershell | Check for xNetworking Powershell module
    win_psmodule:
      name: xNetworking
      state: present


  # == Install .NET framework ==========================================

  - name: Windows | Install .NET Framework Core
    win_feature:
      name: NET-Framework-Core
      state: present
    tags: install_net 

  - name: Windows | Install .NET Framework 3.5
    win_feature:
      name: NET-Framework-Features
      state: present
    tags: install_net

  - name: Windows | Install .NET Framework 4.5 Features
    win_feature:
      name: NET-Framework-45-Features
      state: present
      include_sub_features: True
    tags: install_net

  - name: Windows | Install Windows Process Activation Service
    win_feature:
      name: WAS
      state: present
      include_sub_features: True
 

  # == Create Windows service account for SQL ======================================== 
 
  - name: Active Directory | Ensure SQL Service account is present
    win_user:
      name: "{{ mssql_sqlsvc_account | regex_search('[^\\\\]*$') }}"
      # firstname: "{{ mssql_instance_name }}"
      # surname: SQLSvc
      password: "{{ mssql_sqlsvc_account_pass }}"
      password_never_expires: yes
      user_cannot_change_password: yes
      description: "SQL Service account for {{ inventory_hostname }}\\{{ mssql_instance_name }}"
      state: present
      # path: "{{ mssql_base_ldap_path }}"
      # groups:
      #   - Domain Users
    tags: service_account


  - name: Active Directory | Ensure SQL Agent Service account is present
    win_user:
      name: "{{ mssql_agentsvc_account | regex_search('[^\\\\]*$') }}"
      # firstname: "{{ mssql_instance_name }}"
      # surname: AgentSvc
      password: "{{ mssql_agentsvc_account_pass }}"
      password_never_expires: yes
      user_cannot_change_password: yes
      description: "SQL Agent service account for {{ inventory_hostname }}\\{{ mssql_instance_name }}"
      state: present
      # path: "{{ mssql_base_ldap_path }}"
      # groups:
      #   - Domain Users
    tags: service_account


  # == Download and Install SQL ===========================================

  - name: ensure DL dir is present
    win_file:
      path: "{{ item }}"
      state: directory
    loop:  
      - "{{ mssql_temp_download_path }}"
      - "{{ mssql_installation_path }}"

  - name: Fetch SQL Media Downloader
    win_get_url:
      url: "{{ mssql_installation_source }}"
      dest: "{{ mssql_temp_download_path }}\\SQLServer2017-SSEI-Dev.exe"
      follow_redirects: all
    tags: download_sql

  - name: Use Media Downloader to fetch SQL Installation CABs to {{ mssql_installation_path }}
    win_shell: "{{ mssql_temp_download_path }}\\SQLServer2017-SSEI-Dev.exe /Action=Download /MediaPath={{ mssql_installation_path }} /MediaType=CAB /Quiet"

  # Job will fail if extracted media folder is not empty, quick step to ensure it's empty
  - name: Ensure installation media extraction path is empty
    win_file:
      path: "{{ mssql_installation_path }}\\Media"
      state: absent

  - name: Extract installation media
    win_shell: "{{ mssql_installation_path }}\\SQLServer2017-DEV-x64-ENU.exe /X:{{ mssql_installation_path }}\\Media /Q"

  - name: Install SQL Server
    win_dsc:
      resource_name: SQLSetup
      Action: Install
      UpdateEnabled: True
      SourcePath: "{{ mssql_installation_path }}\\Media"
      InstanceName: "{{ mssql_instance_name }}"
      InstallSharedDir: "{{ mssql_installshared_path }}"
      InstallSharedwowDir: "{{ mssql_installsharedwow_path }}"
      InstanceDir: "{{ mssql_instance_path }}"
      InstallSQLDataDir: "{{ mssql_sqlinstalldata_path }}"
      SQLUserDBDir: "{{ mssql_sqluserdata_path }}"
      SQLUserDBLogDir: "{{ mssql_sqluserlog_path }}"
      SQLTempDBDir: "{{ mssql_sqltempDB_path }}"
      SQLTempDBLogDir: "{{ mssql_sqltempDBlog_path }}"
      Features: "{{ mssql_features }}"
      SQLCollation: "{{ mssql_collation }}"
      BrowserSvcStartupType: "{{ mssql_browsersvc_mode }}"
      SuppressReboot: "{{ mssql_suppress_reboot }}"
      # Service Accounts
      #
      # If the type of the DSC resource option is a PSCredential then 
      # there needs to be 2 options set in the Ansible task definition 
      # suffixed with _username and _password. So we will be providing 
      # two options for these normally single option items.

      # SQL Service Account
      SQLSvcAccount_username: "{{ mssql_sqlsvc_account }}"
      SQLSvcAccount_password: "{{ mssql_sqlsvc_account_pass }}"
      # SQL Agent Service Account
      AgtSvcAccount_username: "{{ mssql_agentsvc_account }}"
      AgtSvcAccount_password: "{{ mssql_agentsvc_account_pass }}"
      # SQL Analysis Services Account
      ASSvcAccount_username: "{{ mssql_assvc_account }}"
      ASSvcAccount_password: "{{ mssql_assvc_account_pass }}"

      # Used when installing on a network path, comment out 
      # SourceCredential_username: "{{ ansible_user }}"
      # SourceCredential_password: "{{ ansible_password }}"

      # LoginMode 
      SecurityMode: SQL
      SAPwd_username: "sa"
      SAPwd_password: "{{ mssql_sa_password }}"

      # System Admins 
      SQLSysAdminAccounts: "{{ mssql_sysadmin_accounts }}"
      # Analysis Services Admins (if installed)
      ASSysAdminAccounts: "{{ mssql_asadmin_accounts }}"
    tags: install_sql


  # == Disable firewall for Demo purpose. Must NOT do this in production. =====

  - name: Disable firewall for Domain, Public and Private profiles
    community.windows.win_firewall:
      state: disabled
      profiles:
      - Domain
      - Private
      - Public
    tags: enable_firewall

  # - name: Firewall | Allow Database Engine for instance
  #   win_dsc:
  #     resource_name: xFirewall
  #     Name: "SQL Server Database Engine instance {{ mssql_instance_name }}"
  #     Program: sqlservr.exe 
  #     Ensure: present
  #     Enabled: True
  #     Profile: "Private,Public,Domain"
  #     Direction: "Inbound"
  #     Action: Allow
  #     Description: "Allows the Database Engine to access the network"  
  #     LocalPort: 1433
  #     Protocol: TCP
  #   tags: configure_firewall

  # - name: Firewall | Allow SQLBrowser for instance
  #   win_dsc:
  #     resource_name: xFirewall
  #     Name: "SQL Server Browser instance {{ mssql_instance_name }}"
  #     Service: SQLBrowser
  #     Ensure: present
  #     Enabled: True
  #     Profile: "Private,Public,Domain"
  #     Direction: "Inbound"
  #     Action: Allow
  #     Description: "Allows the SQL Server Browser to access the network"  
  #   tags: configure_firewall


  # Begin SQL Server configuration
  - name: Enable TCP Connectivity
    win_dsc:
      resource_name: SqlServerNetwork
      InstanceName: "{{ mssql_instance_name }}"
      ProtocolName: tcp
      TcpPort: "{{ mssql_port }}"
      IsEnabled: True
      RestartService: True
    tags: configure_sql

  # - name: Adjust Max Server Memory to {{ mssql_max_server_memory }}
  #   when: mssql_max_server_memory is defined
  #   win_dsc:
  #     resource_name: SqlServerConfiguration 
  #     InstanceName: "{{ mssql_instance_name }}"
  #     ServerName: "{{ ansible_hostname }}"
  #     OptionName: max server memory (MB)
  #     OptionValue: "{{ mssql_max_server_memory }}"
  #     RestartService: False
  #   tags: configure_sql

  # - name: Adjust Min Server Memory to {{ mssql_min_server_memory }}
  #   when: mssql_min_server_memory is defined
  #   win_dsc:
  #     resource_name: SqlServerConfiguration 
  #     ServerName: "{{ ansible_hostname }}"
  #     InstanceName: "{{ mssql_instance_name }}"
  #     OptionName: min server memory (MB)
  #     OptionValue: "{{ mssql_min_server_memory }}"
  #   tags: configure_sql

  # - name: Adjust Max Degree of Parallelism
  #   when: mssql_max_degree_of_parallelism is defined
  #   win_dsc:
  #     resource_name: SqlServerConfiguration 
  #     ServerName: "{{ ansible_hostname }}"
  #     InstanceName: "{{ mssql_instance_name }}"
  #     OptionName: max degree of parallelism
  #     OptionValue: "{{ mssql_max_degree_of_parallelism }}"
  #   tags: configure_sql


  # We can invoke SQL command through Powershell. Not needed since we provided the SA password 
  # at the install time. 

  # - name: Set sa Password
  #   win_shell: |
  #     Invoke-Sqlcmd -Query "ALTER LOGIN [sa] WITH PASSWORD='{{ mssql_sa_password }}'" -ServerInstance localhost\{{ mssql_instance_name }} | ConvertTo-Json

  #   register: reset_result

  # - name: Enable sa account
  #   win_shell: |
  #     Invoke-Sqlcmd -Query "ALTER LOGIN [sa] ENABLE" -ServerInstance localhost\{{ mssql_instance_name }} | ConvertTo-Json

  #   register: reset_result


  handlers:

  - name: reboot windows
    win_reboot:
      reboot_timeout: 3600
      post_reboot_delay: 60
    when: mssql_suppress_reboot == False

  - name: restart sqlagent
    win_service:
      name: "SQLAgent${{ mssql_instance_name|upper }}"
      state: restarted